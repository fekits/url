import { call, put, all, select } from 'redux-saga/effects';

import axios from 'axios';
// 默认数据
export default {
  page: 'home',
  user: {},
  news: {}
};

// 请求用户接口
export function* HOME_GET_USER({ then = () => {} } = {}): any {
  const res: any = yield call(axios.get, '/api/user');
  console.log('HOME_GET_USER', res);
  const { code = 0, data = {} } = res.data;
  if (code === 0) {
    yield put({
      type: '$',
      data: {
        key: 'home.user',
        val: data
      }
    });
  }
  then();
}

// 请求新闻接口
export function* HOME_GET_NEWS({ then = () => {} } = {}): any {
  const res: any = yield call(axios.get, '/api/news');
  console.log('HOME_GET_NEWS', res);
  then();
}

// 请求多个接口
export function* HOME_GET_DATA({ then = () => {} } = {}): any {
  yield all([call(HOME_GET_USER), call(HOME_GET_NEWS)]);
  const res: any = yield select((state) => state.home || {});
  console.log('HOME_GET_DATA', res);
  then();
}
